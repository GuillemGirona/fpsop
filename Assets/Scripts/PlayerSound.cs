﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSound : MonoBehaviour {

	public AudioClip gunSound;
	AudioSource fuenteAudio;

	// Use this for initialization
	void Start () {
		fuenteAudio=GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		
		if (Input.GetMouseButtonDown(0)){
			fuenteAudio.clip = gunSound;
			fuenteAudio.Play();
		}

	}
}

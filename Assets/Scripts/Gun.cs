﻿
using UnityEngine;

public class Gun : MonoBehaviour {

	public float damage = 10f;
	public float range = 100f;
	public float impactForce = 30f;
    public GameObject impactEffect;
    public Camera fpsCam;
	public float fireRate = 15f;

	private float nextTimeToFire = 0f;

    public GameObject ImpactEffect
    {
        get
        {
            return impactEffect;
        }

        set
        {
            impactEffect = value;
        }
    }


    // Update is called once per frame
    void Update () {
		
		if (Input.GetButton("Fire1") && Time.time >= nextTimeToFire)
		{
			nextTimeToFire = Time.time +1f/fireRate;
			Shoot();
		}

	}

	void Shoot()
	{
		RaycastHit hit;
		if (Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, range))
		{
			Debug.Log(hit.transform.name);

			Target2 Target2 = hit.transform.GetComponent<Target2>();

			if (hit.rigidbody != null)
			{
				hit.rigidbody.AddForce(hit.normal*impactForce);
			}

			GameObject impactGO = Instantiate(ImpactEffect, hit.point, Quaternion.LookRotation(hit.normal));
			Destroy(impactGO, 2f);



		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonBehaviour : MonoBehaviour
{

    public void Exit()
    {
        Debug.Log("EXIT");
        Application.Quit();
    }

    public void Play()
    {
        Debug.Log("PLAY");
        SceneManager.LoadScene("MainGame");
    }
}
